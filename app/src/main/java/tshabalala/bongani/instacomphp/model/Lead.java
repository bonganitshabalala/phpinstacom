package tshabalala.bongani.instacomphp.model;

import java.io.Serializable;

public class Lead implements Serializable {

    private String name;
    private String surname;
    private String email;
    private String phone;
    private String date;

    public Lead() {
    }

    public Lead(String name, String surname, String email, String phone, String date) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
