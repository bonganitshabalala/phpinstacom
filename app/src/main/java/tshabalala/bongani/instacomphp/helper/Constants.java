package tshabalala.bongani.instacomphp.helper;

public class Constants {

    //URL
    public static final String CREATE_LEAD_URL = "http://epexegetic-volt.000webhostapp.com/registerLead.php";

    public static final String GET_LEADS_URL = "http://epexegetic-volt.000webhostapp.com/getAllLeads.php";

    public static final String UPDATE_LEAD_URL = "http://epexegetic-volt.000webhostapp.com/updateLead.php";

    public static final String DELETE_LEAD_URL = "http://epexegetic-volt.000webhostapp.com/deleteLead.php";

    //Variables
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    public static final String TAG_SUCCESS = "success";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_LEADS = "leads";

}
