package tshabalala.bongani.instacomphp.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.regex.Pattern;

public class Util {

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isEmailValid(String email) {

        boolean valid;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (Pattern.matches(emailPattern, email))
        {
            valid = true;
        }else {
            valid = false;
        }

        return valid;
    }

    public static boolean validatePhone(String phoneNo) {
        //validate phone numbers of format "1234567890"
        boolean valid ;
        String preg = "((?:\\+27|27)|0)(=99|72|82|73|83|74|84|86|79|81|71|76|60|61|62|63|64|78|)(\\d{7})";

        if (Pattern.matches(preg,phoneNo)) {

            valid = true;
        }else
        {
            valid = false;
        }
        return valid;

    }

}
