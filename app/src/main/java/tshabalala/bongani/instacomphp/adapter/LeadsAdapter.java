package tshabalala.bongani.instacomphp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import tshabalala.bongani.instacomphp.R;
import tshabalala.bongani.instacomphp.model.Lead;


public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.MyViewHolder> {

    private List<Lead> leadList;

    public LeadsAdapter(List<Lead> leadList) {
        this.leadList = leadList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView leadName;
        TextView leadSurname;
        TextView leadEmail;
        TextView leadPhone;
        TextView leadDate;

        MyViewHolder(View view) {
            super(view);

            leadName = view.findViewById(R.id.name);
            leadSurname = view.findViewById(R.id.surname);
            leadEmail = view.findViewById(R.id.email);
            leadPhone = view.findViewById(R.id.phone);
            leadDate = view.findViewById(R.id.date);

        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leads_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Lead lead = leadList.get(position);

        holder.leadName.setText(lead.getName());
        holder.leadSurname.setText(lead.getSurname());
        holder.leadEmail.setText(lead.getEmail());
        holder.leadPhone.setText(lead.getPhone());
        holder.leadDate.setText(lead.getDate());

    }

    @Override
    public int getItemCount() {
        return leadList.size();
    }


    public void removeItem(int position) {
        leadList.remove(position);
        notifyItemRemoved(position);
    }


}