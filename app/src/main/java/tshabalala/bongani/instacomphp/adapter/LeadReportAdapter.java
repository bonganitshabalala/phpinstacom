package tshabalala.bongani.instacomphp.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tshabalala.bongani.instacomphp.R;
import tshabalala.bongani.instacomphp.model.Lead;


public class LeadReportAdapter extends ArrayAdapter<Lead> {

    private Activity mContext;
    private LayoutInflater mInflater;
    List<Lead> leads ;

    private static class ViewHolder {
        private TextView name = null;
        private TextView surname = null;
        private TextView email = null;
        private TextView phone = null;
        private TextView date = null;
    }

    public LeadReportAdapter(Activity context, int textViewResourceId, List<Lead> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        leads = objects;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final Lead lead = leads.get(position);
        Log.w("TAG", "leads "+ lead.getDate() +" string date ");
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.report_item, parent, false);

            vh.name =  contentView.findViewById(R.id.name);
            vh.surname = contentView.findViewById(R.id.surname);
            vh.email =  contentView.findViewById(R.id.email);
            vh.phone =  contentView.findViewById(R.id.phone);
            vh.date =  contentView.findViewById(R.id.date);

            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }

        vh.name.setText(lead.getName());
        vh.surname.setText(lead.getSurname());
        vh.email.setText( lead.getEmail());
        vh.phone.setText(lead.getPhone());
        vh.date.setText(lead.getDate());

        return (contentView);
    }


}