package tshabalala.bongani.instacomphp.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import tshabalala.bongani.instacomphp.R;
import tshabalala.bongani.instacomphp.adapter.LeadReportAdapter;
import tshabalala.bongani.instacomphp.model.Lead;

public class LeadDialogFragment extends DialogFragment {

    ListView listView;
    static List<Lead> leadList;
    static List<String> dateList;
    List<Lead> reportLead = new ArrayList<>();

    public static LeadDialogFragment newInstance(List<Lead> leadLists, List<String> dateLists) {
        Bundle bundle = new Bundle();
        leadList = leadLists;
        dateList = dateLists;
        LeadDialogFragment sampleFragment = new LeadDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.view_lead_report, null);
            final Bundle args = getArguments();

            listView = dialogView.findViewById(R.id.listView);

            for(String date: dateList){

                for(Lead lead : leadList){
                    if(date.equals(lead.getDate())){
                        if(!reportLead.contains(lead)){
                            reportLead.add(lead);
                        }
                    }
                }
            }

            Log.w("TAG", "leads "+ leadList.size() +" string date "+ dateList.size()+ " re "+ reportLead.size());

            LeadReportAdapter leadReportAdapter = new LeadReportAdapter(getActivity(),android.R.string.selectTextMode,reportLead);
            listView.setAdapter(leadReportAdapter);

            if(reportLead.size() == 0){

                Toast.makeText(getActivity(), " No leads for selected dates", Toast.LENGTH_SHORT).show();
            }

            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setPositiveButton("Cancel", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            leadList = new ArrayList<>();
                            dialog.dismiss();

                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

}
