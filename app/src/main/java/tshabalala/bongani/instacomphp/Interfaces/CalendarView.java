package tshabalala.bongani.instacomphp.Interfaces;

public interface CalendarView {

    void navForward();

    void navBack();

    void initialisePager(int initialPos);
}
