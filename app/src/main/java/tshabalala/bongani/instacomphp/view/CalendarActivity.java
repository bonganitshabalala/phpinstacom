package tshabalala.bongani.instacomphp.view;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import tshabalala.bongani.instacomphp.R;
import tshabalala.bongani.instacomphp.fragment.LeadDialogFragment;
import tshabalala.bongani.instacomphp.helper.BottomNavigationViewHelper;
import tshabalala.bongani.instacomphp.model.Lead;

public class CalendarActivity extends AppCompatActivity{

    private List<String> datelist = new ArrayList<>();
    private String date;
    private Button viewReport;
    private List<Lead> leadList;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        viewReport = findViewById(R.id.buttonReport);

        CalendarView calendarView = findViewById(R.id.calendarView);

        leadList = (List<Lead>) getIntent().getSerializableExtra("leads");
        if (calendarView != null) {
            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                    // Note that months are indexed from 0. So, 0 means January, 1 means february, 2 means march etc.
                   String mon = null;
                   if((month + 1) < 10){
                       mon = "0"+(month+1);
                   }
                    date = dayOfMonth+ "/" + mon + "/" + year;

                    Log.w("TAg", "date "+ date);

                    if(!datelist.contains(date)) {
                        datelist.add(date);
                        Toast.makeText(CalendarActivity.this,date +" added",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        viewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LeadDialogFragment leadDialogFragment = LeadDialogFragment.newInstance(leadList, datelist);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Fragment previous = getFragmentManager().findFragmentByTag("newEventDialog");
                if (previous != null){
                    transaction.remove(previous);
                }
                transaction.add(leadDialogFragment, "newEventDialog");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        BottomNavigationView bottomNavigationView =  findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {

            switch (item.getItemId())
            {
                case R.id.navigation_home:
                    Intent view = new Intent(CalendarActivity.this, MainActivity.class);
                    startActivity(view);
                    break;

                case R.id.navigation_calender:

                    break;
            }

            return false;
        });


    }

    private String toDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return sdf.format(c.getTime());
    }


}
