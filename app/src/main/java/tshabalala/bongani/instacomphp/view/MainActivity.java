package tshabalala.bongani.instacomphp.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import tshabalala.bongani.instacomphp.R;
import tshabalala.bongani.instacomphp.adapter.LeadsAdapter;
import tshabalala.bongani.instacomphp.helper.BottomNavigationViewHelper;
import tshabalala.bongani.instacomphp.helper.MyDividerItemDecoration;
import tshabalala.bongani.instacomphp.helper.RecyclerTouchListener;
import tshabalala.bongani.instacomphp.helper.SwipeToDeleteCallback;
import tshabalala.bongani.instacomphp.helper.Util;
import tshabalala.bongani.instacomphp.model.Lead;

import static tshabalala.bongani.instacomphp.helper.Constants.CONNECTION_TIMEOUT;
import static tshabalala.bongani.instacomphp.helper.Constants.DELETE_LEAD_URL;
import static tshabalala.bongani.instacomphp.helper.Constants.GET_LEADS_URL;
import static tshabalala.bongani.instacomphp.helper.Constants.READ_TIMEOUT;
import static tshabalala.bongani.instacomphp.helper.Constants.TAG_LEADS;
import static tshabalala.bongani.instacomphp.helper.Constants.TAG_MESSAGE;
import static tshabalala.bongani.instacomphp.helper.Constants.TAG_SUCCESS;
import static tshabalala.bongani.instacomphp.helper.Constants.UPDATE_LEAD_URL;
import static tshabalala.bongani.instacomphp.helper.Util.isInternetAvailable;
import static tshabalala.bongani.instacomphp.helper.Util.validatePhone;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private CompositeDisposable disposable = new CompositeDisposable();
    private LeadsAdapter leadsAdapter;
    private List<Lead> leadList = new ArrayList<>();

    RecyclerView recyclerView;

    ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.my_recycler_view);

        leadsAdapter = new LeadsAdapter( leadList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(leadsAdapter);



        /**
         * On press on RecyclerView item, open alert dialog
         * with options to Edit lead
         * */
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                editLeadDialog(leadList.get(position));
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        if(isInternetAvailable(this)){

           new fetchAllLeads().execute();
        }else{
            Toast.makeText(MainActivity.this, R.string.internet_connection, Toast.LENGTH_LONG).show();
        }

        BottomNavigationView bottomNavigationView =  findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {

            switch (item.getItemId())
            {
                case R.id.navigation_home:

                    break;

                case R.id.navigation_calender:
                    Intent view = new Intent(MainActivity.this, CalendarActivity.class);
                    view.putExtra("leads", (Serializable) leadList);
                    startActivity(view);
                    break;
            }

            return false;
        });

        enableSwipeToDeleteAndUndo();
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {

            startActivity(new Intent(MainActivity.this, RegisterActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Deleting a post
     * when swiping LEFT OR swiping RIGHT.
     */
    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {


                final int position = viewHolder.getAdapterPosition();
                final Lead lead = leadList.get(position);
                leadsAdapter.removeItem(position);
                new deleteLead(lead).execute();

            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);
    }

    /**
     * Fetching leads
     */
    public class fetchAllLeads extends AsyncTask<String,String,String>
    {

        HttpURLConnection conn;
        URL url = null;

        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Getting leads...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                //URL address
                url = new URL(GET_LEADS_URL);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("GET");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_LEADS);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            String name = jsonObject.getString("name");
                            String surname = jsonObject.getString("surname");
                            String email = jsonObject.getString("email");
                            String phone = jsonObject.getString("phone");
                            String date = jsonObject.getString("date");


                            Lead lead = new Lead(name, surname, email, phone, date);
                            if(lead != null)
                            {
                                if(!leadList.contains(lead)) {
                                    leadList.add(lead);
                                }
                            }


                        }

                        leadsAdapter = new LeadsAdapter( leadList);
                        recyclerView.setAdapter(leadsAdapter);
                        leadsAdapter.notifyDataSetChanged();


                    } else {

                        Toast.makeText(MainActivity.this, "Could not retrieve leads", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    /**
     * Shows alert dialog with EditText options to enter / edit
     * a lead.
     */
    private void editLeadDialog(final Lead lead) {
        View view = LayoutInflater.from(this).inflate(R.layout.edit_dialog, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText etName = view.findViewById(R.id.name);
        final EditText etSurname = view.findViewById(R.id.surname);
        final EditText etPhone = view.findViewById(R.id.phone);
        final EditText etEmail = view.findViewById(R.id.email);

        if (lead != null) {
            etName.setText(lead.getName());
            etSurname.setText(lead.getSurname());
            etPhone.setText(lead.getPhone());
            etEmail.setText(lead.getEmail());
        }
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Update", (dialogBox, id) -> {

                })
                .setNegativeButton("Cancel", (dialogBox, id) -> dialogBox.cancel());

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            // Show toast message when no text is entered
            boolean cancel = false;
            View focusView = null;
            if (TextUtils.isEmpty(etName.getText().toString())) {
                Toast.makeText(MainActivity.this, "Name cant be empty", Toast.LENGTH_SHORT).show();
                focusView = etName;
                cancel = true;
            }

            if (TextUtils.isEmpty(etSurname.getText().toString())) {
                Toast.makeText(MainActivity.this, "Surname cant be empty", Toast.LENGTH_SHORT).show();
                focusView = etSurname;
                cancel = true;
            }

            if (TextUtils.isEmpty(etPhone.getText().toString())) {
                Toast.makeText(MainActivity.this, "Phone number cant be empty", Toast.LENGTH_SHORT).show();
                focusView = etPhone;
                cancel = true;
            }

            if (!TextUtils.isEmpty(etPhone.getText().toString())) {
                if (!validatePhone(etPhone.getText().toString())) {
                    etPhone.setError("Error - Enter correct cellphone");
                    focusView = etPhone;
                    cancel = true;

                }
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the lead registration attempt.
                if (!isInternetAvailable(MainActivity.this)) {
                    Toast.makeText(MainActivity.this, R.string.internet_connection, Toast.LENGTH_LONG).show();
                } else {

                    Lead leads = new Lead(etName.getText().toString(), etSurname.getText().toString(),lead.getEmail(), etPhone.getText().toString(), toDate());
                    // updating lead
                    new updateLead(leads).execute();
                    alertDialog.dismiss();

                }
            }

        });
    }

    /**
     * Updating a lead
     */
    private class updateLead extends AsyncTask<String,String,String>
    {

        Lead lead;

        public updateLead(final Lead lead){

            this.lead = lead;
        }

        HttpURLConnection conn;
        URL url = null;

        protected void onPreExecute() {
        super.onPreExecute();

        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Updating lead...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }

        @Override
        protected String doInBackground(String... param) {

        try {
            //URL address
            url = new URL(UPDATE_LEAD_URL);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "exception";
        }
        try {
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection)url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("POST");
            // setDoInput and setDoOutput method depict handling of both send and receive
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Append parameters to URL
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("name", lead.getName())
                    .appendQueryParameter("surname", lead.getSurname())
                    .appendQueryParameter("phone", lead.getPhone())
                    .appendQueryParameter("email", lead.getEmail());
            String query = builder.build().getEncodedQuery();

            // Open connection for sending data
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            conn.connect();

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return "exception";
        }

        try {

            int response_code = conn.getResponseCode();

            Log.d("response_code",""+ response_code);
            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                Log.d("data",""+ result.toString());
                // Pass data to onPostExecute method
                return(result.toString());

            }else{

                return("unsuccessful");
            }

        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        } finally {
            conn.disconnect();
        }


    }

        @Override
        protected void onPostExecute(String success) {

        pDialog.dismiss();
        if (success != null){
            Log.i("Website Content", success);
            try {
                JSONObject jsonPart = new JSONObject(success);
                int num = jsonPart.getInt(TAG_SUCCESS);
                String message = jsonPart.getString(TAG_MESSAGE);

                if (num == 1) {

                    //TODO update adapter
                   new fetchAllLeads().execute();

                } else {

                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {

                e.printStackTrace();
            }
        }
    }

    }

    /**
     * Deleting a post
     */
    private class deleteLead extends AsyncTask<String,String,String>
    {

        Lead lead;
        public deleteLead(Lead lead){

           this.lead = lead;
        }

        HttpURLConnection conn;
        URL url = null;

        protected void onPreExecute () {
        super.onPreExecute();

        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Deleting lead...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }

        @Override
        protected String doInBackground (String...param){

        try {

            //URL address
            url = new URL(DELETE_LEAD_URL);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "exception";
        }
        try {
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("POST");

            // setDoInput and setDoOutput method depict handling of both send and receive
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Append parameters to URL
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("email", lead.getEmail());
            String query = builder.build().getEncodedQuery();

            // Open connection for sending data
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();


            conn.connect();

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return "exception";
        }

        try {

            int response_code = conn.getResponseCode();

            Log.d("response_code", "" + response_code);
            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                Log.d("data", "" + result.toString());
                // Pass data to onPostExecute method
                return (result.toString());

            } else {

                return ("unsuccessful");
            }

        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        } finally {
            conn.disconnect();
        }


    }

        @Override
        protected void onPostExecute (String success){

        pDialog.dismiss();
        if (success != null) {
            Log.i("Website Content", success);
            try {
                JSONObject jsonPart = new JSONObject(success);
                int num = jsonPart.getInt(TAG_SUCCESS);
                String message = jsonPart.getString(TAG_MESSAGE);

                if (num == 1) {

                    //TODO update adapter
                    new fetchAllLeads().execute();

                } else {

                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    private String toDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return sdf.format(c.getTime());
    }

}
