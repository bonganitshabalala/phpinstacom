package tshabalala.bongani.instacomphp.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import tshabalala.bongani.instacomphp.R;
import tshabalala.bongani.instacomphp.helper.Constants;

import static tshabalala.bongani.instacomphp.helper.Constants.TAG_MESSAGE;
import static tshabalala.bongani.instacomphp.helper.Constants.TAG_SUCCESS;
import static tshabalala.bongani.instacomphp.helper.Util.isEmailValid;
import static tshabalala.bongani.instacomphp.helper.Util.isInternetAvailable;
import static tshabalala.bongani.instacomphp.helper.Util.validatePhone;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etName;
    private EditText etSurname;
    private EditText etEmail;
    private EditText etPhone;

    private String name;
    private String surname;
    private String email;
    private String phone;

    private Button btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();
    }

    private void init() {
        etName = findViewById(R.id.name);
        etSurname = findViewById(R.id.surname);
        etEmail = findViewById(R.id.email);
        etPhone = findViewById(R.id.phone);
        btnCreate = findViewById(R.id.buttonSignUp);
        btnCreate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){

        switch (view.getId()) {
            case R.id.buttonSignUp:
                attemptRegistration();
                break;
        }
    }

    private void attemptRegistration() {

        // Store values at the time of the login attempt.
        name = etName.getText().toString();
        surname = etSurname.getText().toString();
        email = etEmail.getText().toString();
        phone = etPhone.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(name)) {
            etName.setError("Error - name can't be empty");
            focusView = etName;
            cancel = true;

        }

        if (TextUtils.isEmpty(surname)) {
            etSurname.setError("Error - surname can't be empty");
            focusView = etSurname;
            cancel = true;

        }

        if (TextUtils.isEmpty(phone)) {
            etPhone.setError("Error - phone can't be empty");
            focusView = etPhone;
            cancel = true;

        }

        if (!TextUtils.isEmpty(phone)) {
            if (!validatePhone(phone)) {
                etPhone.setError("Error - Enter correct cellphone");
                focusView = etPhone;
                cancel = true;

            }
        }


        if (TextUtils.isEmpty(email)) {
            etEmail.setError("Error - email can't be empty");
            focusView = etEmail;
            cancel = true;

        }

        if (!TextUtils.isEmpty(email)) {
            if (!isEmailValid(email)) {
                etEmail.setError("Error - Enter correct email");
                focusView = etEmail;
                cancel = true;

            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the lead registration attempt.
            if (!isInternetAvailable(RegisterActivity.this)) {
                Toast.makeText(RegisterActivity.this, R.string.internet_connection, Toast.LENGTH_LONG).show();
            } else {

                 registrationTask();

            }
        }
    }

    /**
     * Represents an asynchronous register task used to authenticate
     * the lead.
     */
    public void registrationTask() {

        final ProgressDialog pd = new ProgressDialog(RegisterActivity.this);
        pd.setMessage("Creating lead...");
        pd.show();

        final StringRequest request = new StringRequest(Request.Method.POST, Constants.CREATE_LEAD_URL, s -> {
            Log.w("TAG", " s "+ s);
            if (s != null){

                try {
                    JSONObject jsonPart = new JSONObject(s);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {
                        Intent intent =  new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);

                    } else {
                        Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    pd.dismiss();
                    e.printStackTrace();
                }
            }

            pd.dismiss();
        }, volleyError -> {
            System.out.println("Erroe" + volleyError);
            pd.dismiss();
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> jsonObject = new HashMap<>();
                jsonObject.put("name", name);
                jsonObject.put("surname", surname);
                jsonObject.put("email", email);
                jsonObject.put("phone", phone);
                jsonObject.put("createdDate", toDate());
                return jsonObject;
            }


        };

        RequestQueue rQueue = Volley.newRequestQueue(RegisterActivity.this);
        rQueue.add(request);

    }

    private String toDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return sdf.format(c.getTime());
    }

}